# SOCalenderEvent

Recently, while working on a client’s project, we had to integrate Event kit framework in the project.

We thought to create a demo on it and written a tutorial to [Making a Calendar Event Reminder App Using Event Kit Framework](https://www.spaceotechnologies.com/make-calendar-event-reminder-app-event-kit-framework/).

If you face any issue implementing it, you can contact us for help. Also, if you want to implement this feature in your iOS app and looking to [hire iPhone app developer](http://www.spaceotechnologies.com/hire-iphone-developer/) to help you, then you can contact Space-O Technologies for the same.